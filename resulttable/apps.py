# Convenções conforme https://docs.djangoproject.com/en/2.1/ref/applications/

from django.apps import AppConfig


class ResultConfig(AppConfig):
    name = 'resulttable'
    verbose_name = 'Tabela dos Resultados'
