# Convenções conforme https://docs.djangoproject.com/en/2.1/ref/applications/

from django.apps import AppConfig


class WebFriendsConfig(AppConfig):
    name = 'webfriends'
    verbose_name = "Observatório Virtual"
