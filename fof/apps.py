# Convenções conforme https://docs.djangoproject.com/en/2.1/ref/applications/

from django.apps import AppConfig


class FoFConfig(AppConfig):
    name = 'fof'
    verbose_name = "Algoritmo Friends-of-Friends"
