
<div id="navigation">                                            
    <div id="portal-column-one" class="cell width-1:4 position-0">      
    
	<ul class="menu-info">
        <li><a href="http://www.inpe.br/acessoainformacao/" title="Saiba sobre o Acesso à Informação" target="_blank">Acesso à Informação</a></li>
         <li class="center"><a href="/" title="Acesse INPE"><img class="inpe-menu" src="/img/logoinpe-azul-menor.png" alt="Imagem do INPE" /><span class="none">INPE</span></a></li>
    </ul>
    
       
    <input type="checkbox" id="menu-mobile" class="trigger" />              
    <label for="menu-mobile">Menu <img src="/img/menu.png" alt="Menu" /><span class="clear"><!-- --></span></label>
    
    
    <ul class="cd-accordion-menu">
  		<li>
       	<input type="checkbox" id="1" />
        <label for="1" title="Acesse os subitens de Modelo Padrão" class="titulo">Modelo Padrão</label>
        
        	<ul>
                <li><a href="/" title="Acesse Sobre">Sobre</a></li>
                <li><a href="#" title="Acesse Quem Somos">Quem Somos</a></li>
                <li><a href="#" title="Acesse Banner Rotativo">Banner Rotativo</a></li>
                <li><a href="#" title="Acesse Drop Down">Drop Down</a></li>
                <li><a href="#" title="Acesse Tabelas, Caixas e Layouts">Tabelas, Caixas e Layouts</a></li>
                <li><a href="#" title="Acesse Formulários">Formulários</a></li>
               	<li><a href="#" title="Acesse Download Modelo">Download <br />Modelo<img src="/img/externo.png" alt="Imagem Externo" class="right" /></a></li>
                
                <li><a href="#" title="Acesse Contatos">Contatos</a></li>
    		</ul>
        </li>      
        
	</ul> 
	</div>
</div>    